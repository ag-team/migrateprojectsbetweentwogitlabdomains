#!/bin/bash

# Source and destination GitLab URLs
source_gitlab_url="https://gitlab.com/ag-team"
destination_gitlab_url="https://gitlab.com/newmigration"

migrate_repository(){
	local source_repo_url=$1
    local destination_repo_url=$2

    # Clone repository
    git clone $source_repo_url
    cd $(basename $source_repo_url .git)

    # Add remote for destination repository
    git remote add destination $destination_repo_url

    # Fetch branches and tags from source remote
    git fetch origin

    # Push fetched branches and tags to destination remote
    git push destination --all
    git push destination --tags

    # Go back to the previous directory
    cd ..
	
	# delete project folder in local
	rm -rf $(basename $source_repo_url .git)
}
# Read the project name from user input
read -p "Enter the project name to migrate: " project_name

# Get the source and destination repository URLs
source_repo_url="${source_gitlab_url}/${project_name}.git"
destination_repo_url="${destination_gitlab_url}/${project_name}.git"

echo "Migrating repository: $project_name"

# Migrate the repository
migrate_repository "$source_repo_url" "$destination_repo_url"
